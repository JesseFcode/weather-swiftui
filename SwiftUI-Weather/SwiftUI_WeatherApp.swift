//
//  SwiftUI_WeatherApp.swift
//  SwiftUI-Weather
//
//  Created by Jesse Frederick on 12/19/22.
//

import SwiftUI

@main
struct SwiftUI_WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
