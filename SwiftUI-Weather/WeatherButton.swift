//
//  WeatherButton.swift
//  SwiftUI-Weather
//
//  Created by Jesse Frederick on 12/19/22.
//

import SwiftUI

struct WeatherButton: View {
    var buttonName: String
    var textColor: Color
    var backgroundColor: Color
    var body: some View {
        Text(buttonName)
             .frame(width: 280, height: 50)
             .background(backgroundColor)
             .foregroundColor(textColor)
             .font(.system(size: 20, weight: .bold, design: .default))
             .cornerRadius(10)
    }
}
